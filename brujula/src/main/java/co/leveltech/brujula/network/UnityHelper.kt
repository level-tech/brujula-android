package co.leveltech.brujula.network

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AlertDialog
import es.situm.sdk.model.cartography.Geofence


internal class UnityHelper(private val context: Context, private val host: String) {

    fun openJustGame(tk: String, userId: String, fullName: String) {
        val url = "$host/hub/index.html?tk=$tk&userId=$userId&fullName=$fullName"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        context.startActivity(intent)
    }

    fun showGeofenceInfo(geofences: List<Geofence>) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Geofence Debug")
        builder.setMessage(geofences.toString())
        builder.setPositiveButton("Ok") { dialog, _ ->
            dialog.dismiss()
        }
        builder.show()
    }
}