package co.leveltech.brujulanapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import co.leveltech.brujula.Brujula
import co.leveltech.brujulan.R
import co.leveltech.brujulanapp.helloworld.HelloWorldFragment
import co.leveltech.brujulanapp.map.MapFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private val userId = "johnDoe"
    private val fullName = "John Doe"
    private var apiToken = "5ace1d6372cb69f5132ad7e3662e2905709893e7dbd7b3cc4539fdf01b3c619c"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Brujula.Companion.Builder(
            context = this,
            host = "https://brujulago.malldelsol.info.ec",
            userId = userId,
            fullName = fullName,
            apiToken = null
        ).build()

        setCurrentFragment(HelloWorldFragment())

        findViewById<BottomNavigationView>(R.id.bottomNavigationView).setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.hello_world -> setCurrentFragment(HelloWorldFragment())
                R.id.map -> setCurrentFragment(MapFragment())
            }
            true
        }
    }


    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.container, fragment)
            commit()
        }

    companion object {
        private val TAG = this::class.java.simpleName
    }
}